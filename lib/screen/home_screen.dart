import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testapp/bloc/home_bloc.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<HomeBloc>(
      builder: (context) => HomeBloc()..initShareReceiver(),
      dispose: (context, bloc) => bloc.dispose(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Sharing app test'),
        ),
        body: Consumer<HomeBloc>(builder: (context, homeBloc, _) {
          return Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  StreamBuilder<String>(
                      stream: homeBloc.photoStream,
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? Image.file(File(snapshot.data))
                            : const SizedBox.shrink();
                      }),
                  StreamBuilder<String>(
                      stream: homeBloc.textStream,
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? SelectableText(snapshot.data)
                            : const SizedBox.shrink();
                      }),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
