import 'dart:async';

import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc {
  final _photoReceivedSubject = BehaviorSubject<String>();

  Stream<String> get photoStream => _photoReceivedSubject.stream;

  final _textReceivedSubject = BehaviorSubject<String>();

  Stream<String> get textStream => _textReceivedSubject.stream;

  StreamSubscription _imageOutsideListener;
  StreamSubscription _textOutsideListener;

  void initShareReceiver() {
    // For sharing images coming from outside the app
    // while the app is in the memory
    _imageOutsideListener = ReceiveSharingIntent.getMediaStream().listen(
        (files) {
      if (files?.isNotEmpty ?? false) _photoReceivedSubject.add(files[0].path);
    }, onError: (err) {
      print("getIntentDataStream error: $err");
    });

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia().then((files) {
      if (files?.isNotEmpty ?? false) _photoReceivedSubject.add(files[0].path);
    });

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _textOutsideListener = ReceiveSharingIntent.getTextStream()
        .listen(_textReceivedSubject.add, onError: (err) {
      print("getLinkStream error: $err");
    });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then(_textReceivedSubject.add);
  }

  void dispose() {
    _imageOutsideListener?.cancel();
    _textOutsideListener?.cancel();
    _photoReceivedSubject.close();
    _textReceivedSubject.close();
  }
}
